package commands;

import models.Executable;
import periphery.Processor;

/**
 * Created by shpp-admin on 7/15/2017.
 */
public class LD_r_r implements Executable {

    private Processor proc;

    private int registerTo;
    private int registerFrom;

    public LD_r_r(Processor proc, int registerTo, int registerFrom) {
        this.registerTo = registerTo;
        this.registerFrom = registerFrom;
        this.proc = proc;
    }

    public void execute() {
        System.out.println("setting from " + registerFrom + " to " + registerTo);
        proc.setRegiser(registerTo, proc.getRegister(registerFrom));
    }
}
