package commands;

import models.Executable;
import periphery.Processor;

/**
 * Created by shpp-admin on 7/15/2017.
 */
public class LD_r_n implements Executable {

    private int registerTo;
    private Processor proc;

    public LD_r_n(Processor proc, int registerTo) {
        this.registerTo = registerTo;
        this.proc = proc;
    }

    @Override
    public void execute() {
        System.out.println("executing ld_r_n");

        proc.setRegiser(registerTo, proc.getNextByte());
    }
}
