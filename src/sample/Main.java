package sample;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import periphery.ZxSpectrum;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static utils.Constants.SCREEN_HEIGHT;
import static utils.Constants.SCREEN_WIDTH;

public class Main extends Application {

    ZxSpectrum zxSpectrum;

    @Override
    public void start(Stage primaryStage) throws Exception {
        CanvasPane canvasPane = new CanvasPane(SCREEN_WIDTH, SCREEN_HEIGHT);
        Canvas canvas = canvasPane.getCanvas();

        GraphicsContext g = canvas.getGraphicsContext2D();
        zxSpectrum = new ZxSpectrum(g);

        StackPane root = new StackPane(canvasPane);

        root.setOnDragOver(event -> event.acceptTransferModes(TransferMode.ANY));

        root.setOnDragDropped(event -> {
            File image = event.getDragboard().getFiles().get(0);

            zxSpectrum.execute(image);
            event.consume();
        });

        zxSpectrum.run();

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
