package periphery;

import commands.LD_r_n;
import commands.LD_r_r;
import models.Command;
import models.ProcessResources;

/**
 * Created by shpp-admin on 7/15/2017.
 */
public class CommandGenerator {

    static void generateLD(Command[] commandsList, Processor proc) {


        for (int i = 0; i < 8; i++) { // ld r, r
            for (int j = 0; j < 8; j++) {
                if (i == 6 || j == 6)
                    continue;

                commandsList[64 | i << 3 | j] = new Command(new LD_r_r(proc, i, j), new ProcessResources(1, 4, 1));
            }
        }

        for (int i = 0; i < 8; i++) {
            if (i == 6)
                continue;

            commandsList[i << 3 | 6] = new Command(new LD_r_n(proc, i), new ProcessResources(1, 4, 1));
        }
//
//        for (let i = 0; i < 8; i++) {// ld r, n
//            mCommandsList[i << 3 | 6] = new Command(new LD_r_r(Processor.mCpuRegisters, i, null, null, true, false, " ld r n to register " + i), new ProcessResources(2, 7, 1.75));
//        }
    }

    static void generateAll(Processor proc, Command[] commands) {
        generateLD(commands, proc);
    }
}
