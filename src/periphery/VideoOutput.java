package periphery;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import sun.font.GraphicComponent;
import utils.Tools;

import java.util.Arrays;

import static utils.Constants.*;

/**
 * Created by shpp-admin on 7/15/2017.
 */
public class VideoOutput {

    private int[] memory;
    private GraphicsContext gc;

    public VideoOutput(GraphicsContext gc, int[] memory) {
        this.memory = memory;
        this.gc = gc;
    }

    private void drawByte(int position, int byt, String color, String backgroundColor) {

        if (color == null)
            color = "#000000";

        boolean[] bits = Tools.byteToBitsArray(byt);
        for (int i = 7; i >= 0; i--) {
            if (bits[i])
                gc.setFill(Color.web(color));
            else if (backgroundColor != null)
                gc.setFill(Color.web(backgroundColor));
            else
                continue;

            System.out.println("x = " + (Tools.getXFromPosition(position) + i * SPECTRUM_EMULATOR_PIXELS_RATIO));

            gc.fillRect(Tools.getXFromPosition(position) + i * SPECTRUM_EMULATOR_PIXELS_RATIO, Tools.getYFromPosition(position), SPECTRUM_EMULATOR_PIXELS_RATIO, SPECTRUM_EMULATOR_PIXELS_RATIO);
        }
    }

    private void drawScreen() {
        for (int i = VIDEO_MEMORY_START; i <= VIDEO_MEMORY_END + 1; i++) {
            drawByte(i, memory[i], null, null);
        }
    }

    public void drawColours() {
        for (int i = COLOUR_VIDEO_MEMORY_START; i <= COLOUR_VIDEO_MEMORY_END; i++) {
            drawColor(i, memory[i]);
        }
    }

    private void drawColor(int colourPosition, int number) {
        int bytePosition = Tools.getPixelBytePositionFromColorBytePosition(colourPosition);
        for (int i = 0; i < 8; i++) {
            drawByte(bytePosition, memory[bytePosition], Tools.getColorFromByte(number), Tools.getBackgroundColorFromByte(number));
            bytePosition += 256;
        }
    }
}
