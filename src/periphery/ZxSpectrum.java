package periphery;

import javafx.scene.canvas.GraphicsContext;
import periphery.Processor;
import periphery.VideoOutput;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static utils.Constants.SPECTRUM_MEMORY_LIMIT;
import static utils.Constants.VIDEO_MEMORY_START;

/**
 * Created by shpp-admin on 7/15/2017.
 */
public class ZxSpectrum {

    private VideoOutput videoOutput;
    private Processor processor;

    private int[] memory = new int[SPECTRUM_MEMORY_LIMIT];

    public ZxSpectrum(GraphicsContext graphicsContext) {
        videoOutput = new VideoOutput(graphicsContext, memory);
        processor = new Processor(memory);
    }

    public void run() {
        processor.prepare();
        processor.run();
    }


    private byte[] getBytes(File file) {
        try {
            return Files.readAllBytes(Paths.get(file.getAbsolutePath()));
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("IO exception o.o");
        }
    }

    public void loadScreen(File scrFile) {
        byte[] bytes = getBytes(scrFile);

        for (int i = 0; i < bytes.length; i++)
            memory[VIDEO_MEMORY_START + i] = bytes[i];

        videoOutput.drawColours();
    }

    public void execute(File binaryFile) {
        byte[] bytes = getBytes(binaryFile);

        processor.executeBytes(bytes);
    }
}
