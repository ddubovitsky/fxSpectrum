package periphery;

import models.Command;

import java.util.Arrays;

import static utils.Constants.*;

public class Processor {

    private Command[] commands = new Command[50000];
    private int[] mCpuRegisters = new int[9];
    private int[] memory;

    Processor(int[] memory) {
        this.memory = memory;
    }

    void prepare() {
        CommandGenerator.generateAll(this, commands);
    }

    void run() {
        startExecutingCommands();
    }

    private void startExecutingCommands() {
        while ((memory[mCpuRegisters[PROCESSOR_COUNT]]) != 0) {
            System.out.println("executing " + memory[mCpuRegisters[PROCESSOR_COUNT]]);

            execute(memory[mCpuRegisters[PROCESSOR_COUNT]]);
            mCpuRegisters[PROCESSOR_COUNT] += 1;
            System.out.println("processor count = " + mCpuRegisters[PROCESSOR_COUNT]);
            System.out.printf("A: %d B: %d C: %d D: %d E: %d H: %d L: %d \n",
                    mCpuRegisters[A_REGISTER], mCpuRegisters[B_REGISTER], mCpuRegisters[C_REGISTER], mCpuRegisters[D_REGISTER], mCpuRegisters[E_REGISTER], mCpuRegisters[H_REGISTER], mCpuRegisters[L_REGISTER]);
        }
    }

    private void execute(int command) {
        commands[command].run();
    }

    public void setRegiser(int register, int value) {
        mCpuRegisters[register] = value;
    }

    public int getRegister(int register) {
        return mCpuRegisters[register];
    }

    public int getNextByte() {
        return memory[++mCpuRegisters[PROCESSOR_COUNT]];
    }

    public void executeBytes(byte[] bytes) {
        for (int i = 0; i < bytes.length; i++) {
            memory[i] = bytes[i];
        }

        startExecutingCommands();

        clearAll();
    }

    private void clearAll() {
        for (int i = 0; i < memory.length; i++) {
            memory[i] = 0;
        }

        for (int i = 0; i < mCpuRegisters.length; i++) {
            mCpuRegisters[i] = 0;
        }
    }
}

