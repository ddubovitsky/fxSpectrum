package utils;

import java.util.HashMap;

import static utils.Constants.*;

/**
 * Created by shpp-admin on 7/15/2017.
 */
public class Tools {

    private static HashMap<Integer, String> colorsmap;
    private static HashMap<Integer, String> brightColors;

    public static boolean[] byteToBitsArray(int number) {// maybe it can be simplified, i'll think about it later
        boolean[] bitsArray = new boolean[8];

        int count = 0;

        for (int i = 128; i >= 1; i /= 2) {
            if ((number & i) > 0)
                bitsArray[count] = true;
            else
                bitsArray[count] = false;
            count++;
        }

        return bitsArray;
    }

    public static int getXFromPosition(int position) {// first we mask unwanted bytes, to get to know our position in bytes then we multiply it to pixel count in one byte and then to pixel ratio between real spectrum and emulator
        return (position & 31) * 8 * SPECTRUM_EMULATOR_PIXELS_RATIO;
    }

    public static int getYFromPosition(int position) {// actually I dont know how this function works, but it works right
        position = (position >> 5) & MAX_BYTE_VALUE; // it may help you understand this →→→ http://www.animatez.co.uk/computers/zx-spectrum/screen-memory-layout/
        int result = position & 192; // needs variable for bytes swap
        result |= (position & 7) << 3; // mask unwanted bytes and shift to position
        result |= (position & 56) >> 3; // same as ↑↑↑ but with another bytes
        return result * SPECTRUM_EMULATOR_PIXELS_RATIO;
    }

    public static int getPixelBytePositionFromColorBytePosition(int colorBytePosition) {
        colorBytePosition -= (COLOUR_VIDEO_MEMORY_START);
        int x = (colorBytePosition & 31);
        int y = (colorBytePosition & 993) >> 2;

        return getPixelBytePositionFromXY(x, y);
    }


    public static int getPixelBytePositionFromXY(int positionX, int positionY) {
        int pixelBytePosition = VIDEO_MEMORY_START | positionX;
        pixelBytePosition |= (positionY & 7) << 8;
        pixelBytePosition |= (positionY & 56) << 2;
        pixelBytePosition |= (positionY & 192) << 5;
        return pixelBytePosition;
    }

    public static String getColorFromByte(int number) {
        if (isBright(number))
            return getBrightColor(number & 7);
        else {
            return getColor(number & 7);
        }
    }

    public static String getBackgroundColorFromByte(int number) {
        number >>= 3;
        if (isBright(number))
            return getColor(number & 7);
        else {
            return getBrightColor(number & 7);
        }
    }

    public static boolean isBright(int number) {
        return (number & 64) != 0;
    }


    public static String getColor(int code) {
        if (colorsmap != null)
            return colorsmap.get(code);


        colorsmap = new HashMap<>();

        colorsmap.put(BLACK_COLOR, "#000000");
        colorsmap.put(BLUE_COLOR, "#0000CD");
        colorsmap.put(RED_COLOR, "#CD0000");
        colorsmap.put(MAGENTA_COLOR, "#CD00CD");
        colorsmap.put(GREEN_COLOR, "#00CD00");
        colorsmap.put(CYAN_COLOR, "#00CDCD");
        colorsmap.put(YELLOW_COLOR, "#CDCD00");
        colorsmap.put(WHITE_COLOR, "#CDCDCD");

        return colorsmap.get(code);
    }


    public static String getBrightColor(int code) {
        if (brightColors != null)
            return brightColors.get(code);


        brightColors = new HashMap<>();

        brightColors.put(BLACK_COLOR, "#000000");
        brightColors.put(BLUE_COLOR, "#0000FF");
        brightColors.put(RED_COLOR, "#FF0000");
        brightColors.put(MAGENTA_COLOR, "#FF00FF");
        brightColors.put(GREEN_COLOR, "#00FF00");
        brightColors.put(CYAN_COLOR, "#00FFFF");
        brightColors.put(YELLOW_COLOR, "#FFFF00");
        brightColors.put(WHITE_COLOR, "#FFFFFF");

        return brightColors.get(code);
    }

}
