package utils;

import java.util.HashMap;

/**
 * Created by shpp-admin on 7/15/2017.
 */
public class Constants {
    public static final int VIDEO_MEMORY_START = 16384;
    public static final int VIDEO_MEMORY_END = 22527;

    public static final int COLOUR_VIDEO_MEMORY_START = 22528;
    public static final int COLOUR_VIDEO_MEMORY_END = 23295;

    public static final int SPECTRUM_SCREEN_WIDTH = 256;
    public static final int SPECTRUM_SCREEN_HEIGHT = 192;

    public static final int SPECTRUM_EMULATOR_PIXELS_RATIO = 3;

    public static final int SCREEN_WIDTH = SPECTRUM_SCREEN_WIDTH * SPECTRUM_EMULATOR_PIXELS_RATIO;
    public static final int SCREEN_HEIGHT = SPECTRUM_SCREEN_HEIGHT * SPECTRUM_EMULATOR_PIXELS_RATIO;

    public static final int MAX_BYTE_VALUE = 255;

    public static final int BITS_IN_BYTE = 8;
    public static final boolean DEBUG = true;
    public static final int A_REGISTER = 7;
    public static final int B_REGISTER = 0;
    public static final int C_REGISTER = 1;
    public static final int D_REGISTER = 2;
    public static final int E_REGISTER = 3;
    public static final int H_REGISTER = 4;
    public static final int L_REGISTER = 5;

    public static final int PROCESSOR_COUNT = 8;

    public static final int BLACK_COLOR = 0;
    public static final int BLUE_COLOR = 1;
    public static final int RED_COLOR = 2;
    public static final int MAGENTA_COLOR = 3;
    public static final int GREEN_COLOR = 4;
    public static final int CYAN_COLOR = 5;
    public static final int YELLOW_COLOR = 6;
    public static final int WHITE_COLOR = 7;

    public static final int SPECTRUM_MEMORY_LIMIT = 65535;
}
