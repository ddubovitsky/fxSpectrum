package models;

/**
 * Created by shpp-admin on 7/14/2017.
 */
public class Command {
    private Executable function;
    private ProcessResources resources;

    public Command(Executable f, ProcessResources pr) {
        this.function = f;
        this.resources = pr;
    }

    public void run() {
        function.execute();
        resources.emulate();
    }
}
