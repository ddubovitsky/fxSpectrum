package models;

/**
 * Created by shpp-admin on 7/14/2017.
 */
public interface Executable {
    void execute();
}
